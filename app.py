from flask import Flask, render_template, request
import tomdle

app = Flask(__name__)
game = tomdle.NewGame()

@app.route("/")
def index():
    return render_template("index.html", words=game.words)

@app.route("/try", methods=['GET'])
def try_game():
    # print(word)
    request.args.get('word', 'Word')
    game.try_word(request.args.get('word', 'Word'))
    return render_template("index.html", words=game.words)

if __name__ == "__main__":
    app.run(debug=True, port=3000)
