import random

class NewGame():
    
    word_stock = ['astro', 'lider','metal','perro','aguas', 'timar', 'timon', 'tunel', 'obeso', 'oveja', 'abeja', 'cuota']
    words = {'picked_word' : '_____',
            '1' : '_____',
            '2' : '_____',
            '3' : '_____',
            '4' : '_____',
            '5' : '_____',
            '6' : '_____'}
    user_attempts = 1

    def __init__(self):
        self.run_game()


    def pick_word(self):
        '''
        should pick a word from the list, delente from there and return it
        '''
        words_len = len(self.word_stock)
        n = random.randint(0,words_len-1)
        self.words['picked_word'] = self.word_stock.pop(n)
    
    def try_word(self, word_chosen):
        '''
        should compare a word given with the word choosen and return the words thast match
        '''
        print(word_chosen)
        self.words[str(self.user_attempts)] = word_chosen
        self.user_attempts += 1
        if(word_chosen == self.words['picked_word']):
            print('You win!')
            return True
        for i in range(5):
            if(word_chosen[i]==self.words['picked_word'][i]):
                print(word_chosen[i]+' is in the right position')
            elif(word_chosen[i] in self.words['picked_word']):
                print(word_chosen[i]+' is in the word but in the wrong position')
            else:
                print(word_chosen[i]+' is not in the word')
        
        print('Try again!')
        return False

    def ask_word(self):
        '''
        should ask the user to enter a five letter word
        '''
        word = input('Your '+str(self.user_attempts)+' word: ')
        while(len(word)!= 5):
            word = input('Your '+str(self.user_attempts)+' word (must have 5 letters!): ')
        self.user_word = word
        self.user_attempts +=1

    def run_game(self):
        '''
        should run the entire game
        '''
        self.pick_word()
        print(self.words['picked_word'])

